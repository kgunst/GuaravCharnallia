import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from '../dashboard/dashboard.component';
import { HomeComponent } from '../home/home.component';
import { AddContactComponent } from '../addcontact/addcontact.component';
import { UpdateContactComponent } from '../updatecontact/updatecontact.component';
import { GroupsComponent } from '../groups/groups.component';

const routes: Routes =[
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    { path: 'dashboard',      component: DashboardComponent,
      children: [
        { path: '',               redirectTo: 'home', pathMatch: 'full' },
        { path: 'home',           component: HomeComponent },
        { path: 'addcontact',           component: AddContactComponent },
        { path: 'updatecontact/:phoneNo',           component: UpdateContactComponent },
        { path: 'groups',           component: GroupsComponent }
      ]
    }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
