import { Component, OnInit, Input } from '@angular/core';

import { ContactDetail } from '../shared/datamodels/ContactDetail';
import { CamelCasePipe } from "../shared/custompipes/camelcase.pipe";

@Component({
  selector: 'app-specificdetail',
  templateUrl: './specificdetail.component.html',
  styleUrls: ['./specificdetail.component.css'],
})
export class SpecificDetailComponent implements OnInit {

  @Input()
  specific: ContactDetail;
  constructor() { }

  ngOnInit() {
    
  }

}
