import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { DatabaseCrudService } from '../services/database-crud.service';
import { ModalService } from '../services/modal.service';
import { ContactDetail } from '../shared/datamodels/ContactDetail';

import { FirebaseListObservable } from 'angularfire2/database';

@Component({
  selector: 'app-updatecontact',
  templateUrl: './updatecontact.component.html',
  styleUrls: ['./updatecontact.component.css']
})
export class UpdateContactComponent implements OnInit {

  contactDetails: ContactDetail = new ContactDetail();
  public contacts: FirebaseListObservable<ContactDetail[]>;

  constructor(private dbscrud: DatabaseCrudService, private route: ActivatedRoute, private modalService: ModalService) { }

  ngOnInit() {
    this.contactDetails.phone = +this.route.snapshot.paramMap.get('phoneNo');
    this.contacts = this.dbscrud.getContactDetails();
    this.contacts.subscribe(details => {
      details.forEach(detail => {
        if (detail.phone == this.contactDetails.phone)
          this.contactDetails = detail;
      });
    })
  }

  updateDetails() {
    this.contactDetails.imgurl = "https://image.flaticon.com/icons/png/128/149/149071.png";
    this.dbscrud.updateContactDetails(this.contactDetails);
    this.open('updateContact');
  }

  open(id: string) {
    this.modalService.open(id);
    setTimeout(() => {
      this.modalService.close(id);
    }, 1500);
  }

  close(id: string) {
    this.modalService.close(id);
  }

}
