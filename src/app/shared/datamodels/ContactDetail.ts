export class ContactDetail {
    organization: string;
    phone: number;
    type: string;
    imgurl: string;
    name: string;
    website: string;
    email: string;
    address: string;
    city: string;
    state: string;
    zip: string;
}