import { Pipe } from "@angular/core";
import { PipeTransform } from "@angular/core";

@Pipe({name: 'defaultimage'})
export class DefaultImage implements PipeTransform {

    transform(value: string, fallback: string): string {
        if (value) {
          return value;
        } else {
          return fallback;
        }
    }

}