import { Pipe } from "@angular/core";
import { PipeTransform } from "@angular/core";

@Pipe({name: 'camelcase'})
export class CamelCasePipe implements PipeTransform {

    transform(value: string, args: string[]): any {
        if (!value) return value;
    
        return value.replace(/\w\S*/g, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }

}