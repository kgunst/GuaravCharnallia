import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
// Do not import from 'firebase' as you'd lose the tree shaking benefits
import * as firebase from 'firebase/app';

@Injectable()
export class DatabaseAuthService {

  constructor(public afAuth: AngularFireAuth) { }

  getAuthState() {
    return this.afAuth.authState;
  }

  userLogin() {
    this.afAuth.auth.signInAnonymously()
    .catch(function(err) {
      this.errors += err.message;
    });
  }

}
