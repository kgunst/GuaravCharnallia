import { Injectable } from '@angular/core';

import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { ContactDetail } from '../shared/datamodels/ContactDetail';

@Injectable()
export class DatabaseCrudService {

  private basePath: string = '/ContactDetail';
  public contactDetailsList: FirebaseListObservable<ContactDetail[]> = null;
  constructor(public db: AngularFireDatabase) {  }

  getContactDetails(base=this.basePath, query={}) {
    return this.db.list(base, { query: query });
  }

  addContactDetails(detail: ContactDetail) {
    this.contactDetailsList = this.getContactDetails();
    this.contactDetailsList.update(detail.phone.toString(), detail)
      .catch(error => this.handleErrors(error, "addContactDetails()"));
  }

  updateContactDetails(detail: ContactDetail) {
    this.getContactDetails().update(detail.phone.toString(), detail)
      .catch(error => this.handleErrors(error, "updateContactDetails()"));
  }

  deleteContact(contact: ContactDetail) {
    this.contactDetailsList = this.getContactDetails();
    this.contactDetailsList.remove(contact.phone.toString())
      .catch(error => this.handleErrors(error, "deleteContactDetails()"));
  }

  handleErrors(error: Error, source: string) {
    console.log(error);
    console.log(source);
  }

}
