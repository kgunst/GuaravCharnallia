import { TestBed, inject } from '@angular/core/testing';

import { DatabaseCrudService } from './database-crud.service';

describe('DatabaseCrudService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DatabaseCrudService]
    });
  });

  it('should be created', inject([DatabaseCrudService], (service: DatabaseCrudService) => {
    expect(service).toBeTruthy();
  }));
});
