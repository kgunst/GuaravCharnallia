import { TestBed, inject } from '@angular/core/testing';

import { DatabaseAuthService } from './databaseauth.service';

describe('DatabaseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DatabaseAuthService]
    });
  });

  it('should be created', inject([DatabaseAuthService], (service: DatabaseAuthService) => {
    expect(service).toBeTruthy();
  }));
});
