import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { DatabaseAuthService } from './services/databaseauth.service';
import { DatabaseCrudService } from './services/database-crud.service';
import { ModalService } from './services/modal.service';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { NavbarModule } from './shared/navbar/navbar.module';
import { SidebarModule } from './sidebar/sidebar.module';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HomeComponent } from './home/home.component';
import { AddContactComponent } from './addcontact/addcontact.component';
import { UpdateContactComponent } from './updatecontact/updatecontact.component';
import { GroupsComponent } from './groups/groups.component';

import { CamelCasePipe } from './shared/custompipes/camelcase.pipe';
import { DefaultImage } from './shared/custompipes/defaultimage.pipe';
import { ModalComponent } from './shared/directives/modal.component';

import { environment } from '../environments/environment';

// Do not import from 'firebase' as you'd lose the tree shaking benefits
import * as firebase from 'firebase/app';
import { SpecificDetailComponent } from './specificdetail/specificdetail.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddContactComponent,
    GroupsComponent,
    DashboardComponent,
    UpdateContactComponent,
    SpecificDetailComponent,
    ModalComponent,
    CamelCasePipe,
    DefaultImage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule,
    AppRoutingModule,
    NavbarModule,
    SidebarModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  providers: [ DatabaseAuthService, DatabaseCrudService, ModalService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
