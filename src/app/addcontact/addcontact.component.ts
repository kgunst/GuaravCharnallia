import { Component, OnInit } from '@angular/core';

import { DatabaseCrudService } from '../services/database-crud.service';
import { ModalService } from '../services/modal.service';
import { ContactDetail } from '../shared/datamodels/ContactDetail';

@Component({
  selector: 'app-addcontact',
  templateUrl: './addcontact.component.html',
  styleUrls: ['./addcontact.component.css']
})
export class AddContactComponent implements OnInit {

  contactDetails: ContactDetail = new ContactDetail();

  constructor(private dbscrud: DatabaseCrudService, private modalService: ModalService) { }

  ngOnInit() {
  }

  addDetails(formAddContact) {
    this.contactDetails.imgurl = "https://image.flaticon.com/icons/png/128/149/149071.png";
    this.dbscrud.addContactDetails(this.contactDetails);
    this.open('addedContact');
  }

  open(id: string) {
    this.modalService.open(id);
    setTimeout(() => {
      this.modalService.close(id);
    }, 1500);
  }

  close(id: string) {
    this.modalService.close(id);
  }

}
