import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Do not import from 'firebase' as you'd lose the tree shaking benefits
import * as firebase from 'firebase/app';

import { DatabaseCrudService } from '../services/database-crud.service';
import { ModalService } from '../services/modal.service';
import { ContactDetail } from '../shared/datamodels/ContactDetail';

import { FirebaseListObservable } from 'angularfire2/database';

@Component({
  moduleId: module.id,
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  spec: ContactDetail;
  selected: ContactDetail;
  contacts: FirebaseListObservable<ContactDetail[]>;

  constructor(private dbcrud: DatabaseCrudService, private router: Router, private modalService: ModalService) { }

  ngOnInit() {
    this.contacts = this.dbcrud.getContactDetails();
  }

  viewDetails(contact: ContactDetail) {
    this.spec = contact;
  }

  open(id: string, contact: ContactDetail){
    this.selected = contact;
    this.modalService.open(id);
  }

  close(id: string) {
    this.modalService.close(id);
  }
  deleteContact() {
    this.dbcrud.deleteContact(this.selected);
    this.close('confirmDelete');
  }
}
