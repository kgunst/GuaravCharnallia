// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD3lZh2RqoZDx1_2BB2yp-6nksEcnCUpE0",
    authDomain: "contactmanagergeminid.firebaseapp.com",
    databaseURL: "https://contactmanagergeminid.firebaseio.com",
    projectId: "contactmanagergeminid",
    storageBucket: "contactmanagergeminid.appspot.com",
    messagingSenderId: "503512795253"
  }
};
